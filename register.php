<!DOCTYPE html>
<html>

<head>
    <title>Register</title>
    <meta charset='UTF-8' />
    <meta name='viewport' content='width=device-width, initial-scale = 1.0, shrink-to-fit = no'>

    <style>
        .register {
            max-width: 500px;
            border: 1.5px solid #4e7aa3;
            margin: 0 auto;

        }

        .form-text {
            background-color: #5b9bd5;
            color: #fff;
            padding: 6px;
            width: 200px;
            margin-right: 20px;
            border: 1px solid #4e7aa3;
        }

        .form_name,
        .form_password {
            display: flex;
            margin-top: 20px;
        }
		.form_sex{
        	display: flex;
            margin-top: 20px;
        }
        .form-input {
            border: 1px solid #4e7aa3;
            flex: 1;
            outline: none;
        }

        .container {
            padding: 30px 60px;
        }

        .register_submit {
            margin-top: 30px;

            text-align: center;
        }

        .login-btn {
            background-color: #16b630;
            color: #fff;
            border: 1.5px solid #4e7aa3;
            padding: 10px 20px;
            border-radius: 10px;
            cursor: pointer;
        }
        .select_option{
            color: #4e7aa3;
            padding: 6px;
            width: 200px;
			border: 1px solid #4e7aa3;
        }
        .sex{
        	color: #4e7aa3;
            padding: 6px;
            
        }
        .form-text_sex{
            background-color: #5b9bd5;
            color: #fff;
            padding: 6px;
            width: 170px;
            margin-right: 20px;
            border: 1px solid #4e7aa3;
        }
    </style>
</head>

<body>


    <div class='register'>
        <div class='container'>
            <div class='register_form'>
                <div class='form_name'>
                    <div class='form-text'>Họ và tên</div>
                    <input class='form-input' type='text'>
                </div>
                <div class='form_sex'>
                    <div class='form-text_sex'>Giới tính</div>
                    <?php
                      echo '<form class="sex" method="post">';
                      for($i=0;$i<1;$i++){

                          echo 'Nam <input type="radio" name="gender_'.$i.'[]" value="Nam">Nữ <input type="radio" name="gender_'.$i.'[]" value="Nữ"><br>';
                      } 
                     	 echo '</form>';  
                      ?>
                </div>
                <div class='form_password'>
                    <div class='form-text'>Phân khoa</div>
                    <select class ='select_option' name="khoa" id="khoa">
                        <option value="" selected="selected"> </option>
                            <?php
                              $khoa = array(
                                  ' ' => '    ',
                                  'KDL' => 'Khoa học vật liệu',
                                  'MAT' => 'Khoa học máy tính',
                              );
                              ?>
                        	<?php
                        foreach ($khoa as $key => $val) {
                            $selected = ($vval == ' ') ? 'selected="selected"' : '';
                            echo '<option value="' . $val . '" ' . $selected . '>' . $key . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class='register_submit'>
                <input class='login-btn' type='submit' value='Đăng ký'>
            </div>
        </div>
    </div>

</body>

</html>